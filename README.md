# DictionBot
[![Build Status](https://gitlab.com/shibme/socks5-docker/badges/master/pipeline.svg)](https://gitlab.com/shibme/socks5-docker/pipelines)

A simple docker based SOCKS5 proxy server

### How to start?
Set `TELEGRAM_BOT_TOKEN` environment variable and run the following in your terminal.
```bash
docker run --name socks5-docker --rm -p 8000:8000 shibme/socks5-docker
```
